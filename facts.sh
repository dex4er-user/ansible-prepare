#!/bin/sh

if command -v jq >/dev/null; then
    jq="jq ."
else
    jq="cat"
fi

host=$1
shift

port=${host#*:}
if [ "$host" != "$port" ]; then
    host=${host%:*}
    export ANSIBLE_REMOTE_PORT=$port
fi

hosts=`mktemp`
trap "rm -f $hosts" EXIT
echo $host > $hosts


output=`ansible -i $hosts -u root -m setup $host "$@"`

case "$output" in
    [a-zA-Z0-9]*" | success >> "*) echo "$output" | sed 's/.*| success >> {/{/' | $jq;;
    *) echo $output 1>&2
esac
