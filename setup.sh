#!/bin/sh

host=$1
shift

port=${host#*:}
if [ "$host" != "$port" ]; then
    host=${host%:*}
    export ANSIBLE_REMOTE_PORT=$port
fi

hosts=`mktemp`
trap "rm -f $hosts" EXIT
echo $host > $hosts

ansible-playbook -i $hosts -u root -l $host "$@" `basename $0 .sh`.yml
